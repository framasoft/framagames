// Lien retour Framagames
window.addEventListener('DOMContentLoaded', () => {
  const framagames = document.createElement('a');
  framagames.style = 'position: absolute;z-index:100;background:#fff;top:0px;left:0px;padding:5px 10px;border-radius:0 0 5px 0;font-family: DejaVu Sans !important;color: #333;font-size:14px;text-decoration:none;font-weight:normal;';
  framagames.href = '/';
  framagames.target = '_top';
  framagames.id = 'framagames';
  document.getElementsByTagName('body')[0].appendChild(framagames);
  document.getElementsByTagName('body')[0].style.paddingTop = '25px';
  document.getElementById('framagames').innerHTML = '&larr; Retour sur <b style="color:#725794">Frama</b><b style="color:#cc4e13">games</b>';
});

// Matomo
var _paq = _paq || []; // eslint-disable-line

_paq.push([function matomoCNIL() {
  const self = this;
  function getOriginalVisitorCookieTimeout() {
    const now = new Date();
    const nowTs = Math.round(now.getTime() / 1000);
    const visitorInfo = self.getVisitorInfo();
    const createTs = parseInt(visitorInfo[2], 10);
    const cookieTimeout = 33696000; // 13 months
    const originalTimeout = (createTs + cookieTimeout) - nowTs;
    return originalTimeout;
  }
  this.setVisitorCookieTimeout(getOriginalVisitorCookieTimeout());
}]);

_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);

(function matomoImg() {
  const d = document;
  const g = d.createElement('img');
  const s = d.getElementsByTagName('body')[0];
  g.style = 'position:fixed;border:0;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);';
  g.alt = '';
  g.src = 'https://stats.framasoft.org/p.php?idsite=28&rec=1'; s.appendChild(g);
}());
